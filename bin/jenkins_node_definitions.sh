#!/bin/bash

# Copyright 2015-2020 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

# define Debian build nodes in use for tests.reproducible-builds.org/debian/
# 	FIXME: this is used differently in two places,
#		- bin/reproducible_html_nodes_info.sh
#		  where it *must* only contain the Debian nodes as it's used
#		  to generate the variations… and
#		- bin/reproducible_cleanup_nodes.sh where it would be
#		  nice to also include ionos9+10, to also cleanup
#		  jobs there…
BUILD_NODES="bbx15-armhf-rb.debian.net
cb3a-armhf-rb.debian.net
cbxi4a-armhf-rb.debian.net
cbxi4b-armhf-rb.debian.net
cbxi4pro0-armhf-rb.debian.net
codethink9-arm64.debian.net
codethink10-arm64.debian.net
codethink11-arm64.debian.net
codethink12-arm64.debian.net
codethink13-arm64.debian.net
codethink14-arm64.debian.net
codethink15-arm64.debian.net
codethink16-arm64.debian.net
ff2a-armhf-rb.debian.net
ff2b-armhf-rb.debian.net
ff4a-armhf-rb.debian.net
ff64a-armhf-rb.debian.net
jtk1a-armhf-rb.debian.net
jtk1b-armhf-rb.debian.net
jtx1a-armhf-rb.debian.net
jtx1b-armhf-rb.debian.net
jtx1c-armhf-rb.debian.net
odu3a-armhf-rb.debian.net
odxu4a-armhf-rb.debian.net
odxu4b-armhf-rb.debian.net
opi2a-armhf-rb.debian.net
opi2c-armhf-rb.debian.net
p64b-armhf-rb.debian.net
p64c-armhf-rb.debian.net
ionos1-amd64.debian.net
ionos2-i386.debian.net
ionos5-amd64.debian.net
ionos6-i386.debian.net
ionos11-amd64.debian.net
ionos12-i386.debian.net
ionos15-amd64.debian.net
ionos16-i386.debian.net
wbq0-armhf-rb.debian.net
osuosl167-amd64.debian.net
osuosl168-amd64.debian.net
osuosl169-amd64.debian.net
osuosl170-amd64.debian.net
osuosl171-amd64.debian.net
osuosl172-amd64.debian.net
osuosl173-amd64.debian.net
osuosl174-amd64.debian.net"

NODE_RUN_IN_THE_FUTURE=false
get_node_information() {
	local NODE_NAME=$1
	case "$NODE_NAME" in
	  ionos[56]*|ionos1[56]*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  codethink9*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  codethink11*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  codethink13*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  codethink15*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  osuosl170*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  osuosl172*)
	    NODE_RUN_IN_THE_FUTURE=true
	    ;;
	  *)
	    ;;
	esac
}
