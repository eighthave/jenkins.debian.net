#!/bin/bash
# vim: set noexpandtab:

# Copyright 2020 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

cat << EOF

###########################################################################################
###											###
### This prototype is meant to achieve several goals:					###
###											###
### One goal is to polish /usr/bin/debrebuild from src:devscripts to enable anyone to   ###
### independently verify that a distributed Debian binary package comes from the source ###
### package it's said to be coming from.						###
###											###
### Once this goal has been achieved we can document these steps. currently the answer	###
### to the question "how can I verify installed package X is reproducible?" is: "it's   ###
### really complicated" (and sometimes impossible or requires not yet written code).    ###
###											###
### Another goal is to create json export to integrate in tracker.d.o and/or		###
### packages.d.o as well as to provide statistics and graphs.				###
###											###
### The aim is to develop a 'real world' view about the reproducibility of all the      ###
### packages distributed via ftp.d.o. - so far tests.r-b.o/debian only shows the 	###
### 'theoretical' reproducibility of Debian packages.                                   ###
###											###
### We'll leave out the problem of 'trust' here quite entirely. that's why it's called	###
### a Debian rebuilder 'thing', to explore technical feasibility, duck taping our way	###
### ahead, keeping our motto 'to allow anyone to independently verify...' in mind.	###
###											###
###########################################################################################

EOF


DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh
set -e

output_echo() {
	echo "###########################################################################################"
	echo
	echo -e "$(date -u) - $1"
	echo
}

set_poolpath() {
	local PKG=$1
	if [ "${PKG:0:3}" = "lib" ] ; then
		POOLPATH="${PKG:0:4}"
	else
		POOLPATH="${PKG:0:1}"
	fi
}

#
# define what to rebuild
#

#
# find a random package in bullseye
#
ARCH=amd64
SUITE=bullseye
DISTNAME="$SUITE-$ARCH"
PACKAGES=$(ls $CHPATH/$DISTNAME/var/lib/apt/lists/*_dists_${SUITE}_main_binary-${ARCH}_Packages)
SOURCES=$(ls $CHPATH/$DISTNAME/var/lib/apt/lists/*_dists_${SUITE}_main_source_Sources)
BINARY_PKG=$(grep-available . -s Package $PACKAGES | sort -R | head -1 | cut -d ' ' -f2)
PKG=$( ( grep-available -X -S $BINARY_PKG -s Package $SOURCES || grep-available -X -P $BINARY_PKG -s Source $PACKAGES ) | cut -d ' ' -f2)
VERSION=$(grep-available -X -S $PKG -s Version $SOURCES | head -1 | cut -d ' ' -f2)

#
# hardcoded test cases
#
#PKG=libofx		; BINARY_PKG=libofx-dev 	; VERSION=1:0.9.15-3	# cannot find libdebconfclient0/0.250/amd64 in dumpavail -> impossible to recreate build environment
#PKG=wpewebkit		; BINARY_PKG=wpewebkit-driver	; VERSION=2.28.0-1	# cannot find libc-bin/2.29-10/amd64 in dumpavail -> impossible to recreate build environment
#PKG=libunibreak	; BINARY_PKG=libunibreak1	; VERSION=1.1-2		# binNMU (on amd64 in bullseye, but not buster)
#PKG=tomcat9		; BINARY_PKG=tomcat9-user	; VERSION=9.0.37-3	# sbuild fails to build the package because dpkg-checkbuilddeps: error: Unmet build dependencies: - see job #924

output_echo "let's try to rebuild $BINARY_PKG from src:$PKG ($VERSION) from $SUITE/$ARCH"
EVERSION="$(echo $VERSION | cut -d ':' -f2)"  # EPOCH_FREE_VERSION is too long

# query .buildinfo file path (for that $PKG and $VERSION from bullseye...) from builtin-pho db
BUILTINPHOPATH=$(psql -A -t -c "SELECT path FROM builds WHERE source='$PKG' AND source_version='$VERSION' AND ( arch_amd64=true OR arch_all=true ) LIMIT 1" buildinfo)
if [ -z "$BUILTINPHOPATH" ] ; then
	output_echo "No .buildinfo file found for $PKG ($VERSION) for $ARCH - check https://tracker.debian.org/$PKG"
	OTHER_FILES=$(mktemp)
	psql -A -t -c "SELECT path FROM builds WHERE source='$PKG' AND (arch_amd64=true OR arch_all=true)" buildinfo > $OTHER_FILES
	if [ -s $OTHER_FILES ] ; then
		echo ".buildinfo files known for $PKG and (arch_amd64 or arch_all)"
		echo
		cat $OTHER_FILES
	else
		echo "Warning: no .buildinfo exists for $PKG ($VERSION) and (arch_amd64 or arch_all)."
		echo
		psql -A -t -c "SELECT path FROM builds WHERE source='$PKG'" buildinfo > $OTHER_FILES
		if [ -s $OTHER_FILES ] ; then
			echo ".buildinfo files known for $PKG:"
			echo
			cat $OTHER_FILES
		else
			echo  "Warning: found no .buildinfo files for $PKG in builtin-pho db at all, let's query dak."
			rmadison -S -a $ARCH,all,source -s $SUITE,sid $PKG
		fi
	fi
	rm -f $OTHER_FILES
	output_echo "Warning: NBIFA - no .buildinfo file for $PKG ($VERSION on $ARCH) available. Exiting cleanly as this is out-of-scope."
	exit 0
fi
output_echo "Found $BUILTINPHOPATH"
FILE="$(echo $BUILTINPHOPATH | cut -d '/' -f5-)"
PKG="$(echo $FILE | cut -d '_' -f1)"
BINARY_VERSION="$(echo $FILE | cut -d '_' -f2)"
POOLPATH=""            # declared as a global variable
set_poolpath $PKG      # so we can set it here with a function
FTPMASTERPATH="$(echo $BUILTINPHOPATH | cut -d '/' -f2-4)"
URLPATH="https://buildinfos.debian.net/ftp-master.debian.org/buildinfo/$FTPMASTERPATH"

if [ "$VERSION" != "$BINARY_VERSION" ] && [ "$EVERSION" != "$BINARY_VERSION" ] ; then
	output_echo "Warning: .buildinfo file known for $PKG and $VERSION is $URL_PATH/$FILE: this indicates a binNMU because $VERSION != $BINARY_VERSION."
elif [ "$VERSION" != "$EVERSION" ] ; then
	: # package has an epoch
fi

# FIXME: we need locking to enable concurrent job runs

# FIXME: hack, should be done properly, with cleanup *after* the job run...
mkdir -p $PWD/rebuilder-prototype
BTPKG="$PWD/rebuilder-prototype/$PKG"
mkdir $BTPKG 2>/dev/null || (rm $BTPKG -r ; mkdir $BTPKG)
cd $BTPKG

#
# main: this is basically a description of the steps to use debrebuild today...
#

output_echo "downloading $URLPATH/$FILE"
curl $URLPATH/$FILE > $FILE
if head -1 $FILE | grep -q 'BEGIN PGP SIGNED MESSAGE' ; then
	TMPFILE=$(mktemp -t debrebuild-buildinfo.XXXXXXXX)
	cp $FILE $TMPFILE
	# workaround #955050 in devscripts: debrebuild: please accepted signed .buildinfo files
	cat $TMPFILE | gpg > $FILE || true # we cannot validate the signature and we don't care
	rm $TMPFILE
	echo
	output_echo  "$URLPATH/$FILE with gpg signature stripped:"
else
	output_echo  "$URLPATH/$FILE is unsigned:"
fi
cat $FILE

# download the source early to fail early if it's not available...
output_echo "fetching source package $PKG ($VERSION)"
# just download the source, don't verify it. (keys will expire and be removed from the keyrings)
# FIXME: debrebuild should download the source code too (--optionally) and verify it matches the one described in .buildinfo file. -> this is #961861
#        except that this won't work, contrary to what we suggested, the .buildinfo files don't contain hashes of the source package built. bummer!
# FIXME: debrebuild also lacks a feature to assemble the source for binNMUs, see #961862
dget --download-only --allow-unauthenticated https://deb.debian.org/debian/pool/main/$POOLPATH/$PKG/${PKG}_$EVERSION.dsc
dscverify ${PKG}_$EVERSION.dsc || echo "Warning: failed to verify signature, continueing anyway." # FIXME: we can verify most sources in a Debian release, but never all. basically because keys expire. IOW: this is a pretty fundamental basic problem, for which I know no answer, so let's ignore it and move on ;) for now.

# prepare rebuild command
DEBREBUILD=$(mktemp -t debrebuild-cmd.XXXXXXXX)
output_echo "trying to debrebuild $PKG ($BINARY_VERSION), which means building instructions how to re-create the build environment as specified in $URLPATH/$FILE"
# workaround: we are using rb-debrebuild here which is a code copy from debrebuild from devscripts.git
# using --no-respect-build-path is a workaround until sbuild 0.81 is available. see https://salsa.debian.org/debian/devscripts/-/merge_requests/203#note_192500
/srv/jenkins/bin/rb-debrebuild --no-respect-build-path $FILE 2>&1 | tee $DEBREBUILD

# FIXME: file a bug like '#955123 debrebuild: please provide --sbuild-output-only option' but with --output-only-base-release
# (parsing the debrebuild output to gather this information is way to fragile)
DISTRO=$(tail -1 $DEBREBUILD | grep '^BASE_DIST=' | cut -d '=' -f2)
case $DISTRO in
	stretch|buster|bullseye|bookworm|unstable) ;;
	*)	output_echo "debrebuild failed."
		if egrep -q 'cannot find .* in dumpavail' $DEBREBUILD ; then
			#FIXME: file bug, debrebuild should fail clearly on this (and not this subtile)
			echo "The following build-dependencies are not available on snapshot.debian.org:"
			echo
			egrep 'cannot find .* in dumpavail' $DEBREBUILD
			echo
			echo "Warning: this should not happen and it's unclear why it did: $(egrep 'cannot find .* in dumpavail' $DEBREBUILD) - still exiting cleanly as this is out-of-scope."
			exit 0
		elif egrep -q 'Cannot locate the following packages via snapshots or the current repo/mirror' $DEBREBUILD ; then
			echo "Some packages could not be located, check the log above."
			echo
			echo "Warning: this should not happen and it's unclear why it did - still exiting cleanly as this is out-of-scope."
			exit 0
		else
			echo "Something went wrong with debrebuild, please take a look what exactly..."
			echo
			exit 1
		fi
		;;
esac

# create chroot for sbuild
if [ -d /schroots/debrebuild-$DISTRO-$ARCH ] ; then
	output_echo "chroot for $DISTRO/$ARCH exists, good."
else
	output_echo "preparing chroot for $DISTRO/$ARCH."
	# FIXME: "|| true" is dummy code for regenerating this chroot every other week or so
	sudo sbuild-createchroot $DISTRO /schroots/debrebuild-$DISTRO-$ARCH http://deb.debian.org/debian || true

	# I'm a bit surprised this was needed, as debrebuild has code for this...
	# FIXME: a bug should probably be filed for this as well
	echo 'Acquire::Check-Valid-Until "false";' | sudo tee /schroots/debrebuild-$DISTRO-$ARCH/etc/apt/apt.conf.d/23-rebuild
fi

# a successful build might overwrite the original .buildinfo file, so move it away
mv $FILE $FILE.orig

# actually run sbuild
# - workaround #955123 in devscripts: debrebuild: please provide --sbuild-output-only option
#   - using tail
# - workaround yet unfiled bug in devscript by tail -2 | grep -v BASE_DIST... (see above)
# - no solution yet to rebuild using the same path as the original build: see #964722
output_echo "trying to re-sbuild $PKG..."
SBUILD=$(tail -2 $DEBREBUILD | grep '^SBUILD_CMDLINE=' | cut -d '=' -f2- )
output_echo "using this sbuild command line:"
echo "$SBUILD"
echo
set +e
eval "$SBUILD"
RESULT=$?
if [ "$RESULT" != "0" ] ; then
	output_echo "sbuild exited with $RESULT:"
	LOGFILE="./${PKG}_${VERSION}_${ARCH}.build"
	if [ -L $LOGFILE ] ; then
		cat $LOGFILE
	else
		output_echo "and no sbuild logfile found:"
		ls -lart
	fi
	output_echo "Warning: sbuild failed. Exiting cleanly as this is out-of-scope."
	exit 0
fi
set -e

# show what we did/created
output_echo "File artifacts:"
ls -lart
output_echo "Diff between .buildinfo files:"
diff $FILE.orig $FILE || true
output_echo "The following binary packages could be rebuilt bit-by-bit identical to the ones distributed from ftp.debian.org:"
# FIXME: this is irrelevant. instead the status of packages in the buildinfo file should be computed
# (not the once we just build...)
BADDEBS=""
for DEB in $(dcmd ls *.changes|egrep 'deb$' ) ; do
	SHASUM=$(sha256sum $DEB | awk '{ print $1 }')
	if grep $SHASUM $FILE.orig ; then
		# reproducible, yay!
		:
		# FIXME: NEXT: put this in the db and prevent rebuilds (way before)...
	else
		BADDEBS="$BADDEBS $DEB"
	fi
done
if [ -n "$BADDEBS" ] ; then
	output_echo "Unreproducible binary packages found:"
	for DEB in $BADDEBS ; do
		echo " $(egrep ' [a-z0-9]{64} ' $FILE.orig|grep $DEB | awk ' { print $1 " " $3 }') from ftp.debian.org"
		echo " $(sha256sum $DEB| sed 's#  # #') from the current rebuild"
		echo "hmmmmmpppf."
	done
fi

# the end
rm -f $DEBREBUILD
output_echo "the end."
